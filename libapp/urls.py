from django.urls import path
from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("view_book/<int:isbn>/", views.view_book, name="view_book"),
    path("categories/", views.categories, name="categories"),
    path("view_category/<str:name>/", views.view_category, name="view_category"),
    path("request_book/<str:isbn>/", views.request_book, name="request_book"),
    path("login/", views.student_login, name="student_login"),
    path("register/", views.student_registration, name="student_registration"),
    path("profile/", views.student_profile, name="student_profile"),
    path("notifications/<int:idnumber>/", views.notifications, name="notifications"),
    path("view_my_books/<int:idnumber>/", views.view_my_books, name="view_my_books"),
    path("request_book/<str:isbn>/", views.request_book, name="request_book"),
    path("return_book/<str:isbn>/", views.return_book, name="return_book"),
    path("search/", views.search_book, name="search_book"),
    path("logout/", views.logoutuser, name="logoutuser"),
]