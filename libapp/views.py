from django.shortcuts import render
from .models import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect, render,HttpResponse
from datetime import date

# Create your views here.
def index(request):
    books = Book.objects.all()
    return render(request, "home.html", {'books':books})

def view_book(request, isbn):
    bookdetails = Book.objects.get(isbn=isbn)
    return render(request, "view_book.html", {'bookdetails':bookdetails})

def categories(request):
    categories = Category.objects.all()
    return render(request, "categories.html", {'categories':categories})

def view_category(request, name):
    category_books = Book.objects.filter(category=name)
    return render(request, "view_category.html", {'category_books':category_books,'title':name})

@login_required(login_url = '/login')
def request_book(request, isbn):
    issued_book = Issuedbook.objects.filter(isbn=isbn)
    if issued_book != "":
        message 
    else:
        obj = models.IssuedBook()
        obj.student_id = request.user.id
        obj.isbn = isbn
        obj.save()

    return render(request, "request_book.html", {'message'})

@login_required(login_url = '/login')
def student_profile(request):
    return render(request, "profile.html")

def student_login(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)

        if user is not None:
            login(request, user)
            if request.user.is_superuser:
                return HttpResponse("You are not a student!!")
            else:
                return redirect("/")
        else:
            alert = True
            return render(request, "login.html", {'alert':alert})
    return render(request, "login.html")

def student_registration(request):
    if request.method == "POST":
        username = request.POST['username']
        fullname = request.POST['full_name']
        email = request.POST['email']
        phone = request.POST['phone']
        course = request.POST['course']
        password = request.POST['password']
        idnumber = request.POST['idnumber']

        user = User.objects.create_user(username=username, email=email, password=password)
        student = Student.objects.create(user=user, full_name=fullname, course=course, id_nnumber=idnumber, phone=phone)
        user.save()
        student.save()
        alert = True
        return render(request, "register.html", {'alert':alert})
    return render(request, "register.html")

def notifications(request, idnumber):
    notifications = Notification.objects.filter(student_id=idnumber)
    return render(request, "notifications.html", {'notifications':notifications})

def view_my_books(request, idnumber): 
    issuedBooks = IssuedBook.objects.filter(student_id=idnumber)

    bookdetails = []
    for i in issuedBooks:
        days = (i.expiry_date-i.issued_date)
        d=days.days
        fine=0
        if d<=-3:
            fine=5000
        elif d<=-15:
            fine=15000
        books = list(Book.objects.filter(isbn=i.isbn))
        students = list(Student.objects.filter(id_nnumber=i.student_id))
        i=0
        for l in books:
            t=(students[i].user,students[i].user_id,books[i].name,books[i].isbn,issuedBooks[0].issued_date,issuedBooks[0].expiry_date,fine,d)
            i=i+1
            bookdetails.append(t)

    return render(request, "view_my_books.html", {'issuedBooks':issuedBooks, 'bookdetails':bookdetails})

def request_book(request, isbn):
    checkbook = IssuedBook.objects.filter(isbn=isbn)
    bookdetails = Book.objects.get(isbn=isbn)
    if checkbook.exists():
        msg = "Sorry, This book is not available at the moment"
        return render(request, "request_book.html", {'msg':msg})
    else:
        obj = IssuedBook.objects.create(student_id=request.user.student.id_nnumber, bookname=bookdetails.name, isbn=isbn)
        obj.save()
        msg = "You have booked this book successfully."
        return render(request, "request_book.html", {'msg':msg})

def return_book(request, isbn):
    checkbook = IssuedBook.objects.filter(isbn=isbn)
    if checkbook.exists():
        checkbook.delete()
        msg = "You returned the book."
        return render(request, "return_book.html", {'msg':msg})
    else:
        msg = "Sorry, an error occurred. You probably haven't requested for this book."
        return render(request, "return_book.html", {'msg':msg})

def search_book(request):
    searchvalue = request.GET.get("keyword")
    results = Book.objects.filter(name__icontains=searchvalue)
    return render(request, "search.html", {'results':results})

def logoutuser(request):
    logout(request)
    return redirect ("index")