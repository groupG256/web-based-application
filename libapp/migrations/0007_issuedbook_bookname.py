# Generated by Django 3.1.5 on 2022-08-09 21:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('libapp', '0006_auto_20220809_1428'),
    ]

    operations = [
        migrations.AddField(
            model_name='issuedbook',
            name='bookname',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
