from django.db import models
from django.contrib.auth.models import User
from datetime import datetime,timedelta

# Create your models here.
class Admin(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=50)

    def __str__(self):
        return self.user.username

class Book(models.Model):
    name = models.CharField(max_length=200)
    author = models.CharField(max_length=200)
    isbn = models.PositiveIntegerField()
    category = models.CharField(max_length=50)
    image = models.ImageField(upload_to="book-images")

    def __str__(self):
        return str(self.name) + " ["+str(self.isbn)+']'

class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=200, null=True)
    course = models.CharField(max_length=10)
    id_nnumber = models.CharField(max_length=3, blank=True)
    phone = models.CharField(max_length=10, blank=True)
    joined_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.user) + " ["+str(self.course)+']' + " ["+str(self.id_nnumber)+']' + " ["+str(self.phone)+']'

class Category(models.Model):
    title = models.CharField(max_length=200)
    
    def __str__(self):
        return self.title


def expiry():
    return datetime.today() + timedelta(days=7)

class IssuedBook(models.Model):
    student_id = models.CharField(max_length=100, blank=True) 
    bookname = models.CharField(max_length=200, null=True)
    isbn = models.PositiveIntegerField()
    issued_date = models.DateField(auto_now=True)
    expiry_date = models.DateField(default=expiry)

class Notification(models.Model):
    message = models.TextField()
    student_id = models.CharField(max_length=3, blank=True)