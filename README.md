WEB-BASED APPLICATION
This is  a web-based application that a enables student to request for books from a library catalog. The library  contains a set of books showing the title, publication date, subject area, author etc. The
Librarian has an interface to post the books and the student is able to search for a book and request for it. A book once requested needs to have a return date that is automatically tracked. A book is unavailable if it has been given out, and only becomes available when returned. Before the return date (1 day) the borrower  gets a notification, by email to return the book. A book returned 3 days after the return date attracts a penalty of 5,000 UGX. If it is returned after ten days the penalty is 15,000 UGX. The system  displays a report that shows the books that need to be returned, when they should be returned and the penalty.
 The system has search area where a student can search for any books and it also has suggestion space for new editions of books.
 Before a student choses the book the system provides a preview to see if the book is exactly of the specifications the student wants 
 it also has a reading basket for books that a user may want to read borrow later 
 Accounts of students who want to have specific privileges are available in the system.
 The system has a place book order section in which an order for a book from a student is obtained and prepared for pickup or download.
 The system is designed using a python web framework called Django.
Authors: groupG computer science classs year one makerere University